angular.module('demo').config(['$routeProvider', function ($routeProvider) {

	$routeProvider
		.when('/demo/hello_world', {
			templateUrl: 'app/demo/hello_world/hello_world.html',
			controller: 'HelloWorldCtrl'
  		})
		.when('/demo/home', {
			templateUrl: 'app/demo/home/home.html',
			controller: 'HomeController'
		})
		.when('/demo/list', {
	    		templateUrl: 'app/demo/list/list.html',
			controller: 'ListCtrl'
		})
		.when('/marvel/creator/list', {
			templateUrl: 'app/marvel/creator/creator_list.html',
			controller: 'CreatorCtrl'
		});
}]);
