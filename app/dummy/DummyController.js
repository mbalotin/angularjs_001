(function(){
    'use strict';

    angular.module('demo').controller('DummyController', DummyController);

    DummyController.$inject = ['$scope'];

    function DummyController($scope) {
        $scope.dummy = function () {
             console.log('test.');
        };
    };
})();