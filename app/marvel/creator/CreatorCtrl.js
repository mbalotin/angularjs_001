(function(){
	'use strict';

	angular.module('demo.marvel.creator').controller('CreatorCtrl', CreatorCtrl);
	CreatorCtrl.$inject=['$scope', 'CreatorSrv'];

	function CreatorCtrl($scope, CreatorSrv) {
		$scope.listCreators = function(){
			// TODO promisse
			var succ = function(result){
				$scope.result = result.data.data.results;
				//alert('success!');
			}
			var fail = function(result){
				alert('Fail!');
			}
			var promisse = CreatorSrv.listCreators();
			promisse.then(succ, fail);
		}
	};
})();
;
