(function(){
	'use strict';

	angular.module('demo.marvel.creator').service('CreatorSrv', CreatorSrv);
	CreatorSrv.$inject=['$http'];

	function CreatorSrv($http) {
		this.listCreators = function(){
			return $http({
				Method: 'GET',
				url: 'http://gateway.marvel.com:80/v1/public/creators?apikey=0e25f6dba33177263f5033d7e697832a'
			});
		}
	};
})();
