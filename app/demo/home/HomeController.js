(function(){
	'use strict';

	angular.module('demo').controller('HomeController', HomeController);
	HomeController.$inject=['$scope'];

	function HomeController($scope) {
		$scope.initHome = function () {
			$scope.people = [
				{
					"name": "Connie Mitchell",
					"address": "Unit 6798 Box 0977\nDPO AA 79824"
				},
				{
					"name": "Benjamin Turner",
					"address": "77983 Smith Junctions\nPort Kara, ME 55157-9546"
				},
				{
					"name": "Chad Andrews",
					"address": "223 Scott Streets Apt. 350\nNew Joseph, NM 27285"
				},
				{
					"name": "Kyle Young",
					"address": "27535 Miller Forks\nSouth Emily, IA 47352"
				},
				{
					"name": "Melissa Cunningham",
					"address": "96714 Castaneda Stream\nSelenamouth, ME 15383-0405"
				},
				{
					"name": "Ian Adams",
					"address": "5622 Day Haven Suite 127\nEast Timothy, IN 79718-5824"
				},
				{
					"name": "Dakota Kennedy",
					"address": "7932 Brady Viaduct\nNew James, KY 21837"
				},
				{
					"name": "Mr. Brad Howard",
					"address": "120 Wesley Point\nNorth Stephanie, IL 79800-5898"
				},
				{
					"name": "Jerry Garcia",
					"address": "31738 Dunlap Ridges\nNew Carolynbury, NY 48467"
				},
				{
					"name": "Johnny Suarez",
					"address": "67494 Russo Ports\nHernandezshire, PR 53338"
				},
				{
					"name": "Alexander Hudson",
					"address": "7943 David Wells Apt. 962\nAndersenhaven, MT 76857"
				},
				{
					"name": "Thomas Rodriguez",
					"address": "72768 Donna Neck\nNorth Jenniferchester, SC 80700"
				},
				{
					"name": "Christina Phillips",
					"address": "12912 Brooke Plains Suite 886\nLake Tracy, CA 82332-4946"
				},
				{
					"name": "Russell Kim II",
					"address": "71705 Marquez Inlet\nAlvaradoport, WV 00832-8948"
				},
				{
					"name": "Joseph Bennett",
					"address": "83881 Alvarado Club Suite 862\nNew Paulmouth, MA 63840"
				},
				{
					"name": "Eric Hall",
					"address": "284 Jones Keys\nTaylormouth, MI 80973"
				},
				{
					"name": "Sean Reeves",
					"address": "PSC 2884, Box 9871\nAPO AE 69471-7471"
				},
				{
					"name": "Andrew Sanchez",
					"address": "644 Smith Fields\nChristinastad, ME 28341"
				},
				{
					"name": "Cynthia Miller",
					"address": "84352 Cook Road Apt. 057\nDavidmouth, DC 92232"
				},
				{
					"name": "Kelly Dougherty",
					"address": "600 Danielle Forge Suite 480\nKathystad, MP 23226-5636"
				},
				{
					"name": "David Gordon",
					"address": "927 Kyle Squares\nHornville, RI 37278-1025"
				},
				{
					"name": "Marissa Collins",
					"address": "84657 Kimberly Harbor Apt. 795\nEast Heather, AR 28158"
				},
				{
					"name": "Colleen Barnett",
					"address": "025 Bates Unions\nPort Kristenmouth, PA 62120-6929"
				},
				{
					"name": "Robert Sanders",
					"address": "092 Spears Club Apt. 554\nNorth Matthew, IL 11334-1413"
				},
				{
					"name": "Jaime Santiago",
					"address": "0226 Michael Hollow\nNorth Kristentown, FL 85716"
				},
				{
					"name": "Margaret Shannon",
					"address": "502 Rodgers Highway\nWest Melissafurt, UT 35983-6795"
				},
				{
					"name": "Tammy Cole",
					"address": "Unit 8170 Box 0857\nDPO AE 18138-8344"
				},
				{
					"name": "Andrea Hess",
					"address": "9726 Taylor Freeway\nEast Brittney, MA 20275-5016"
				},
				{
					"name": "William Sanders",
					"address": "79991 Dennis Valleys\nJonathanview, DC 10863"
				},
				{
					"name": "John Riley",
					"address": "443 Vargas Brook\nNorth Robertberg, MA 94388-2646"
				},
				{
					"name": "Juan Rosario",
					"address": "422 Lisa Ferry\nJoanshire, CT 55005-2465"
				},
				{
					"name": "Taylor Duncan",
					"address": "54953 Alexandra Island Apt. 958\nNew Matthew, ID 01708-1212"
				},
				{
					"name": "Karen Lucero",
					"address": "473 Herrera Turnpike\nNicholasstad, NE 06132"
				},
				{
					"name": "Robert Lopez",
					"address": "2430 Wood Divide\nGriffithfurt, ID 47453-0111"
				},
				{
					"name": "Matthew Campbell",
					"address": "1635 Smith Circles\nChrisside, WI 73770"
				},
				{
					"name": "Terry Gardner",
					"address": "449 Christopher Coves Apt. 889\nNorth Davidshire, GU 57313-4058"
				},
				{
					"name": "Charles Frost",
					"address": "19395 Travis Valley Apt. 327\nWest Carolberg, RI 68662"
				},
				{
					"name": "Tiffany Tucker",
					"address": "6409 Teresa Overpass Suite 667\nTaraville, OH 15630-1517"
				},
				{
					"name": "Patrick Carter",
					"address": "70618 Erin Mills Suite 599\nNew Jaimestad, AZ 23524"
				},
				{
					"name": "Janet Kirk",
					"address": "49445 Haley Camp\nAlicialand, ME 00617"
				},
				{
					"name": "Tracy Miller",
					"address": "74361 Walker Courts Suite 720\nLewisview, NJ 09972"
				},
				{
					"name": "Shannon Cortez",
					"address": "823 Johnson Shores Suite 983\nEast Cameron, AK 92478-4282"
				},
				{
					"name": "Nathan Chambers",
					"address": "72156 Norma Plaza Suite 056\nThomasfurt, IN 43311"
				},
				{
					"name": "Alexandra Hughes",
					"address": "0786 Christopher Coves Suite 696\nShaffertown, CT 40510-3351"
				},
				{
					"name": "Larry Graham",
					"address": "22322 Armstrong Isle Apt. 266\nMartinezland, MN 91527"
				},
				{
					"name": "Brian Phillips MD",
					"address": "USCGC Curtis\nFPO AP 70912-4381"
				},
				{
					"name": "Julie Walsh",
					"address": "4762 Yoder Mission Suite 112\nLindabury, GA 58967-4117"
				},
				{
					"name": "Curtis Johnson",
					"address": "5031 Trevor Ports\nWilliamsmouth, NJ 38438-6380"
				},
				{
					"name": "Lori Freeman",
					"address": "64614 Adam Harbors Suite 763\nDianaland, HI 86458-0080"
				},
				{
					"name": "Alexandra Roman",
					"address": "USNS Wright\nFPO AP 07535"
				}
			];
		};
	};
})();
